# Changelog

## v3.4.0 (unreleased)

* Feature: Automatic backup & restore scripts for mongo & influx
* Feature: Pre and post action hooks

## v3.3.1 (6 November 2018)

* Bugfix: Correctly check if DNS is enabled when polling for zones
* Bugfix: Fix issue when destroying vSphere machines

## v3.3.0 (23 October 2018)

* Feature: Support for block storage volumes in GCE, EC2, OpenStack, DigitalOcean
* Feature: Automatic db migrations
* Feature: Display org logo in user menu
* Feature: Resize GCE machines
* Feature: Allow to create multiple interfaces and assign static IPs to KVM guest VMs
* Feature: Implement VM cloning for KVM
* Feature: Support snapshots in vSphere
* Feature: Allow to enable/disable and edit the window/frequency of a NoDataRule
* Feature: Saved searches in log listings
* Feature: Cloud listing page
* Change: Use html5 date & time inputs
* Change: Improve performance when applying patches to the model over websocket
* Change: Reduce poller update threshold to 90 secs
* Change: Add k8s deployment specific script that displays online portal users
* Bugfix: Properly pass params when running scheduled scripts
* Bugfix: Display prices in resize dialog correctly for DigitalOcean

## v3.2.0 (6 Aug 2018)

Performance optimizations, preliminary support for Alibaba Cloud (Aliyun ECS), minor bugfixes

* Feature: Initial support for Alibaba Cloud
* Feature: Configure default post deploy steps for selected clouds
* Feature: Support VNC console action on OpenStack
* Bugfix: Fix influxdb mountpath in helm chart
* Bugfix: Fix VCloud OS detection
* Bugfix: Fix vSphere machine listing issue
* Bugfix: Fix load graph for many machines
* Change: Expose more settings for rabbitmq, gocky, cilia in helm chart
* Change: Upgrade gocky images
* Change: Configurable pollers in helm chart
* Change: Add flag to disable machine polling acceleration
* Change: Optimize tag query when calculating machine cost
* Change: Re-implement metering for checks and datapoints based on rate/sec
* Change: Dont probe stopped machines or localhost
* Change: Dont run update_poller task if not necessary
* Change: Import middleware chains from plugins, if defined
* Change: Make scheduler configurable in bin/poller


## v3.1.0 (24 Jul 2018)

Adds polling for networks. Improves KVM machine creation & networking. Optimize vSphere API queries. Improves DB query performance. Upgrades ELK which lowers RAM requirements.

### Changes

* Feature: Support custom sizes when creating KVM machines.
* Feature: Store & display resource creator & owner
* Feature: Allow to undefine a KVM domain, if it is not active
* Feature: Support multiple interfaces and attaching to more than one networks when creating KVM machines.
* Feature: Poller for networks.
* Change: Sharding of polling tasks.
* Change: Deprecate collectd support.
* Change: Support metering of datapoints.
* Change: Add owner index, improves performance of DB queries.
* Change: Upgrade ELK to 5.6.10
* Change: Get vm properties in batches in vSphere driver
* Bugfix: Fix internal server error when editing some rules.
* Bugfix: Fix KVM networks upon machine creation.
* Bugfix: Prevent setting telegraf deployment error to "None"
* Bugfix: Do not schedule MeteringPollingSchedule task immediately, since that would result in the task being scheduled every time the Organization instance is saved
* Bugfix: Fix bug regarding incidents not closing when disabling monitoring


## v3.0.0 (29 Apr 2018)

Major new release of ClearGLASS Community.

ClearGLASS now integrates with Telegraf & InfluxDB to provide a fully open source infrastructure management & monitoring stack. It also includes a revamped alerting & automation engine that will apply your rules to any group of machines. We enhanced the support of many clouds, most notably vSphere, GCE & OpenStack. It's now possible to bring together machines into a single virtual "Cloud". The usability and performance of the UI was greatly improved. At the same time we've remorselessly eliminated more than a few bugs.

A new plugin system was introduced, which is currently used by the Enterprise Edition and the Hosted Service to provide add-on functionality like i) Role Based Access Control, ii) Cost Insights, iii) VPN tunnels, iv) Orchestration of multi-tier architectures like Kubernetes clusters, as well as v) metering & billing extensions.


### Changes

* Feature: Machine monitoring, using InfluxDB & Telegraf
* Feature: Alerting and automation rules on machine metrics, apply rules on groups of machines
* Feature: Interactive API docs using OpenAPI 3.0 spec & Swagger UI
* Feature: Poller for cloud locations & sizes
* Feature: Select network & subnetwork when creating GCE machine
* Feature: Support ClearCenter SDN as cloud
* Change: Improved vSphere support
* Change: UI performance improvements
* Change: Support for plugins

